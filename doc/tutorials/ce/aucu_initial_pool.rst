.. _aucu_tutorial_initial_pool:
.. module:: ase.clease.newStruct


Generating initial pool of training structures
==============================================

After the cluster expansion setting is specified, the next step is to generate
initial structures to start training the CE model. New structures for training
CE model are generated using :class:`NewStructures` class, which contains
several methods for generating structures. The initial pool of structures is
generated using :meth:`generate_initial_pool` method::

  from ase.clease import Concentration, CEBulk, NewStructures

  conc = Concentration(basis_elements=[['Au', 'Cu']])
  setting = CEBulk(crystalstructure='fcc',
                   a=3.8,
                   supercell_factor=64,
                   concentration=conc,
                   db_name="aucu_bulk.db",
                   max_cluster_size=4,
                   max_cluster_dia=[6.0, 4.5, 4.5],
                   basis_function='sanchez')

  ns = NewStructures(setting=setting, generation_number=0,
                     struct_per_gen=20)

  ns.generate_initial_pool()

The :class:``NewStructures`` class takes setting instance and two additional
arguments.
* ``generation_number`` is used to track at which point you generated the
structures.
* ``struct_per_gen`` specifies the maximum number of structures to be
generated for that generation number.

The :meth:`generate_initial_pool` method first generates one structure per
concentration where the number of each constituing element is at its
maximum/minimum. In the case of Au-Cu alloy, there are two extrema: Au and Cu.
Consequently, :meth:`generate_initial_pool` first generates 2 structures for
training. It subsequently generates 18 random structures (remaining number of
structures in generation 0) by calling :meth:`generate_random_structures`
method.

Optionally, you can specify the size of the supercell of the random structures
by passing an ::class::`Atoms` object to ``atoms`` argument. For example, you
can use script like below to enforce the supercell size of the random
structures to be :math:`3 \times 3 \times 3`::

  from ase.clease import NewStructures
  from ase.db import connect

  db = connect("aucu.db")
  # get template with the cell size = 3x3x3
  template = db.get("name=template,size=3x3x3").toatoms()

  ns = NewStructures(setting=setting, generation_number=0,
                     struct_per_gen=20)

  ns.generate_initial_pool(atoms=template)


The generated structures are automatically stored in the database with several
key-value pairs specifying their features. The genereated keys are:

.. list-table::
   :header-rows: 1

   * - key
     - description
   * - ``gen``
     - generation number
   * - ``struct_type``
     - ''initial'' for input structures, "final" for converged structures after calculation
   * - ``size``
     - size of the supercell
   * - ``formula_unit``
     - reduced formula unit representation independent of the cell size
   * - ``name``
     - name of the structure (``formula_unit`` followed by a number)
   * - ``converged``
     - Boolean value indicating whether the calculation of the structure is converged
   * - ``queued``
     - Boolean value indicating whether the calculation is queued in the workload manager
   * - ``started``
     - Boolean value indicating whether the calculation has started


The ``queued`` and ``started`` keys are useful for tracking the status of the
calculations submitted using a workload manager, but they do not serve any
purpose in this tutorial.


.. autoclass:: NewStructures
   :members: generate_initial_pool, generate_random_structures
